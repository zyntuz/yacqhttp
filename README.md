**English** - [中文](https://github.com/zyntuz/yacqhttp/blob/master/README.zh.md#yacqhttp)

# yacqhttp

[![npm version](https://img.shields.io/npm/v/yacqhttp.svg)](https://www.npmjs.com/package/yacqhttp)
[![coverage status](https://img.shields.io/coveralls/github/zyntuz/yacqhttp.svg)](https://coveralls.io/github/zyntuz/yacqhttp)
[![GitHub license](https://img.shields.io/npm/l/yacqhttp)](https://gitlab.com/zyntuz/yacqhttp/blob/master/LICENSE)

This is a lightweight [Node.js](https://nodejs.org) package that allows you to interact with the [CoolQ HTTP API](https://github.com/richardchien/coolq-http-api). It can only act as the WebSocket server at the moment. Other methods of communication are not supported.

## Features

- Full TypeScript support.
- Simple and easy to use.
- Load balancing between multiple CoolQ instances.

## Installation

This package requires Node.js v10 or newer and CQHTTP API v4.5.0 or newer.

```bash
$ npm install yacqhttp
```

## Usage example

```js
const { Server } = require('yacqhttp');
const coolQ = new Server();

coolQ
  .on('ready', client => console.log(`${client.id} is ready!`))
  .on('message', (client, event) => client.send(event, 'Hi~'));

coolQ.listen('0.0.0.0', 8080);
```

### With `CommandRegistry`

```js
// Previous example here...

const { CommandRegistry } = require('yacqhttp');
const cmdReg = new CommandRegistry(coolQ);

cmdReg.add({
  name: 'echoFirstWord',
  process(convo, args) {
    convo.reply(`Your first word is: ${args[0]}`);
  },
});
```

## Documentation

See <https://zyntuz.gitlab.io/yacqhttp>.
