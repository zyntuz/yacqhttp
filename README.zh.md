[English](https://github.com/zyntuz/yacqhttp/blob/master/README.md#yacqhttp) - **中文**

# yacqhttp

[![npm version](https://img.shields.io/npm/v/yacqhttp.svg)](https://www.npmjs.com/package/yacqhttp)
[![build status](https://img.shields.io/travis/com/zyntuz/yacqhttp.svg)](https://travis-ci.com/zyntuz/yacqhttp)
[![coverage status](https://img.shields.io/coveralls/github/zyntuz/yacqhttp.svg)](https://coveralls.io/github/zyntuz/yacqhttp)
[![GitHub license](https://img.shields.io/github/license/zyntuz/yacqhttp.svg)](https://github.com/zyntuz/yacqhttp/blob/master/LICENSE)

这是一个用于与 [CoolQ HTTP API](https://github.com/richardchien/coolq-http-api) 交互的轻量级 [Node.js](https://nodejs.org) 包。目前仅支持反向 WebSocket 通讯方法。

## 特色

- 完整的 TypeScript 支持。
- 简单易用。
- 多个 CoolQ 实例之间的负载平衡。

## 安装

本项目依赖 Node.js v10 或更高版本以及 CQHTTP API v4.5.0 或更高版本.

```bash
$ npm install yacqhttp
```

## 用法示例

```js
const { Server } = require('yacqhttp');
const coolQ = new Server();

coolQ
  .on('ready', client => console.log(`${client.id} is ready!`))
  .on('message', (client, event) => client.send(event, 'Hi~'));

coolQ.listen('0.0.0.0', 8080);
```

### 使用 `CommandRegistry`

```js
// Previous example here...

const { CommandRegistry } = require('yacqhttp');
const cmdReg = new CommandRegistry(coolQ);

cmdReg.add({
  name: 'echoFirstWord',
  process(convo, args) {
    convo.reply(`Your first word is: ${args[0]}`);
  },
});
```

## 文档

请参阅 <https://zyntuz.github.io/yacqhttp>。
