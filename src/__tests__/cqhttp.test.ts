import WebSocket from 'ws';
import { Server } from '../server';
import { MsgEvent } from '../cqapi';
import { Client } from '../client';
import { qq, normalGroupMsg, privateMsg, discussMsg, heartbeat } from './data';

describe('CqHttp 1', () => {
  let server: any;
  let api: WebSocket;
  let event: WebSocket;

  beforeEach(done => {
    server = new Server().listen('localhost', 0);
    server.on('socket.listening', () => {
      api = new WebSocket(`ws://localhost:${server.address.port}`, {
        headers: { 'X-Self-ID': qq.str, 'X-Client-Role': 'API' },
      });
      event = new WebSocket(`ws://localhost:${server.address.port}`, {
        headers: { 'X-Self-ID': qq.str, 'X-Client-Role': 'Event' },
      });
      done();
    });
  });

  afterEach(done => {
    server.close(done);
  });

  test('should have correct selfId', done => {
    server.on('ready', (client: Client) => {
      expect(client.id).toBe(qq.int);
      server.close(done);
    });
  });

  test('should successfully call an action', async done => {
    api.once('message', msg => {
      const payload = JSON.parse(msg);
      expect(payload.action).toBe('send_private_msg');
      expect(payload.params).toEqual({ user_id: 1, message: 'hi' });
      api.send('this should be ignored');
      api.send(JSON.stringify({ status: 'ok', echo: payload.echo }));
    });
    server.on('ready', async (client: Client) => {
      await client.do('send_private_msg', {
        user_id: 1,
        message: 'hi',
      });
      done();
    });
  });

  test('client send method should work with GroupMsg', done => {
    const event = normalGroupMsg;
    api.once('message', msg => {
      const payload = JSON.parse(msg);
      expect(payload.action).toBe('send_msg');
      expect(payload.params.group_id).toBe(event.group_id);
      expect(payload.params.message).toBe('test');
      expect(payload.params.auto_escape).toBe(true);
      api.send(JSON.stringify({ status: 'ok', echo: payload.echo }));
    });
    server.on('ready', async (client: Client) => {
      await client.send(event, 'test', true);
      done();
    });
  });

  test('client send method should work with DiscussMsg', done => {
    const event = discussMsg;
    api.once('message', msg => {
      const payload = JSON.parse(msg);
      expect(payload.action).toBe('send_msg');
      expect(payload.params.discuss_id).toBe(event.discuss_id);
      api.send(JSON.stringify({ status: 'ok', echo: payload.echo }));
    });
    server.on('ready', async (client: Client) => {
      await client.send(event, 'test', true);
      done();
    });
  });

  test('client send method should work with MsgEvent', done => {
    const event = privateMsg;
    api.once('message', msg => {
      const payload = JSON.parse(msg);
      expect(payload.action).toBe('send_msg');
      expect(payload.params.user_id).toBe(event.user_id);
      expect(payload.params.group_id).toBeUndefined();
      api.send(JSON.stringify({ status: 'ok', echo: payload.echo }));
    });
    server.on('ready', async (client: Client) => {
      await client.send(event, 'test');
      done();
    });
  });

  test('client send method should not work with MetaEvent', done => {
    const event = heartbeat;
    server.on('ready', async (client: Client) => {
      client.send(event, 'test').catch(() => {
        done();
      });
    });
  });

  test('should receive event', async done => {
    const payload = {
      post_type: 'message',
      message_type: 'group',
      sub_type: 'normal',
      message: 'ping',
    };
    server.on('message', (_: Client, event: MsgEvent) => {
      expect(event).toEqual(payload);
      done();
    });
    event.on('open', () => {
      event.send(Buffer.from('zxcvb'));
      event.send(JSON.stringify(payload));
    });
  });

  test('should ignore connections without role', async done => {
    const ws = new WebSocket(`ws://localhost:${server.address.port}`, {
      headers: { 'X-Self-ID': qq.str },
    });
    ws.on('close', () => {
      done();
    });
  });

  test('should ignore connections without id', async done => {
    const ws = new WebSocket(`ws://localhost:${server.address.port}`);
    ws.on('close', () => {
      done();
    });
  });
});

describe('CqHttp 2', () => {
  test('promise timeout should work', done => {
    const server: any = new Server({ timeout: 1000 }).listen(
      'localhost',
      0,
      () => {
        new WebSocket(`ws://localhost:${server.address.port}`, {
          headers: { 'X-Self-ID': qq.str, 'X-Client-Role': 'Event' },
        });
        new WebSocket(`ws://localhost:${server.address.port}`, {
          headers: { 'X-Self-ID': qq.str, 'X-Client-Role': 'API' },
        });
      },
    );

    server.on('ready', (client: Client) => {
      client
        .do('send_private_msg', {
          user_id: 1,
          message: 'hi',
        })
        .catch(() => {
          server.close(done);
        });
    });
  });

  test('echo should not overflow', () => {
    const server: any = new Server();
    server.echoId = 9999999;
    server.echo;
    expect(parseInt(server.echo)).toBeLessThan(10);
  });

  test('client do should be rejected if API is not available', done => {
    const client = new Client(1, new Server());
    client.do('can_send_image').catch(() => done());
  });

  test('client close will close the corresponding socket', done => {
    let i = 0;
    const onclose = () => {
      i++;
      if (i > 1) server.close(done);
    };

    const server: any = new Server().listen('localhost', 0);
    server.on('socket.listening', () => {
      const api = new WebSocket(`ws://localhost:${server.address.port}`, {
        headers: { 'X-Self-ID': qq.str, 'X-Client-Role': 'API' },
      });
      api.on('close', onclose);
      const event = new WebSocket(`ws://localhost:${server.address.port}`, {
        headers: { 'X-Self-ID': qq.str, 'X-Client-Role': 'Event' },
      });
      event.on('close', onclose);
      server.on('ready', (client: Client) => {
        client.close();
      });
    });
  });

  test('emits an error if port already in use', done => {
    const s1: any = new Server().listen('localhost', 0, () => {
      const s2 = new Server().listen('localhost', s1.address.port);
      s2.on('socket.error', () => s1.close(done));
    });
  });

  test('token should work with both Token and Bearer', done => {
    const server: any = new Server({ token: 'h2o' }).listen(
      'localhost',
      0,
      () => {
        new WebSocket(`ws://localhost:${server.address.port}`, {
          headers: {
            'X-Self-ID': qq.str,
            'X-Client-Role': 'API',
            Authorization: 'Token h2o',
          },
        });
        new WebSocket(`ws://localhost:${server.address.port}`, {
          headers: {
            'X-Self-ID': qq.str,
            'X-Client-Role': 'Event',
            Authorization: 'Bearer h2o',
          },
        });
      },
    );
    server.on('ready', () => server.close(done));
  });
});
