import {
  GroupMsgEvent,
  PrivateMsgEvent,
  DiscussMsgEvent,
  HeartbeatMetaEvent,
} from '../cqapi';

export const qq = {
  str: '123456',
  int: 123456,
};

const basicEvent = {
  time: 3456986,
  self_id: 192,
};

export const normalGroupMsg: GroupMsgEvent = {
  ...basicEvent,
  anonymous: null,
  group_id: 1329,
  user_id: 11,
  font: 1,
  message: 'tata',
  message_id: 876543,
  message_type: 'group',
  post_type: 'message',
  raw_message: 'tata',
  sub_type: 'normal',
  sender: {
    age: 0,
    user_id: 11,
    area: '',
    card: '',
    level: '',
    nickname: 'Pigeon',
    role: 'member',
    sex: 'unknown',
    title: '',
  },
};

export const privateMsg: PrivateMsgEvent = {
  ...basicEvent,
  user_id: 9875423,
  font: 1,
  message: 'hello there~',
  message_id: 456786,
  message_type: 'private',
  post_type: 'message',
  raw_message: 'hello there~',
  sub_type: 'friend',
  sender: {
    age: 0,
    user_id: 9875423,
    nickname: 'Pigeon',
    sex: 'male',
  },
};

export const discussMsg: DiscussMsgEvent = {
  ...basicEvent,
  discuss_id: 8765432,
  user_id: 134351,
  font: 1,
  message: 'yas',
  message_id: 34567,
  message_type: 'discuss',
  post_type: 'message',
  raw_message: 'yas',
  sender: {
    age: 0,
    user_id: 134351,
    nickname: 'Pigeon',
    sex: 'unknown',
  },
};

export const heartbeat: HeartbeatMetaEvent = {
  ...basicEvent,
  post_type: 'meta_event',
  meta_event_type: 'heartbeat',
  status: {
    app_enabled: true,
    app_good: true,
    app_initialized: true,
    good: true,
    online: true,
    plugins_good: {},
  },
};
