import WebSocket from 'ws';
import { Server } from './server';
import { Actions } from './types';
import { Event, CqApiMsg, SendMsgRes } from './cqapi';

type Optional<T> = T extends undefined ? [] : [T];

function parseData(data: any) {
  try {
    return JSON.parse(data);
  } catch {
    return;
  }
}

/**
 * Represents a CoolQ instance created by [[Server]] that has its own ID
 */
export class Client {
  private api?: WebSocket;
  private event?: WebSocket;

  /**
   * Returns true if api socket and event socket are connected
   */
  get ready(): boolean {
    return this.api !== undefined && this.event !== undefined;
  }

  constructor(readonly id: number, private server: Server) {}

  addEvent(ws: WebSocket) {
    this.event = ws;
    ws.on('message', data => {
      const msg = parseData(data);
      if (!msg) return;

      const seg1: string = msg.post_type;
      const seg2: string = msg[`${seg1}_type`];
      const seg3: string | undefined = msg.sub_type;
      if (seg3) this.server.emit(`${seg1}.${seg2}.${seg3}`, this, msg);
      this.server.emit(`${seg1}.${seg2}`, this, msg);
      this.server.emit(seg1, this, msg);
    });
  }

  addApi(ws: WebSocket) {
    this.api = ws;
    ws.on('message', data => {
      const msg = parseData(data);
      if (!msg) return;

      this.server.apiStore.add(msg.echo, msg);
    });
  }

  /**
   * Calls a standard action that is defined in [[Actions]]
   */
  do<K extends keyof Actions>(
    action: K,
    ...params: Optional<Actions[K][0]>
  ): Promise<Actions[K][1]>;
  do(action: string, params = {}) {
    if (!this.api)
      return new Promise((_, reject) => {
        reject('API not available');
      });

    const echo = this.server.echo;
    this.api.send(JSON.stringify({ action, params, echo }));

    return this.server.apiStore.get(echo);
  }

  /**
   * Sends a message where the receiver is decided based on the event parameter
   */
  send(event: Event, message: CqApiMsg, escaped = false): Promise<SendMsgRes> {
    if (event.post_type === 'meta_event') {
      return new Promise((_, reject) => {
        reject('send can not use MetaEvent');
      });
    }

    return this.do('send_msg', { ...event, message, auto_escape: escaped });
  }

  close() {
    if (this.api) {
      this.api.terminate();
      this.api = undefined;
    }
    if (this.event) {
      this.event.terminate();
      this.api = undefined;
    }
  }
}
