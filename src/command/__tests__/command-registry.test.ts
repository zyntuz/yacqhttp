import WebSocket from 'ws';
import { Convo } from '..';
import { Server } from '../../server';
import { CommandRegistry } from '../command-registry';

const payload = {
  post_type: 'message',
  message_type: 'group',
  sub_type: 'normal',
  message: '.dice',
  raw_message: '.dice',
};

test('CommandRegistry should work', done => {
  const server: any = new Server().listen('localhost', 0);
  server.on('socket.listening', () => {
    const api = new WebSocket(`ws://localhost:${server.address.port}`, {
      headers: { 'X-Self-ID': '111', 'X-Client-Role': 'API' },
    });
    api.on('message', (data: any) => {
      const msg = JSON.parse(data);
      expect(msg.params.message).toBe('4');
      api.send(JSON.stringify({ status: 'ok', echo: msg.echo }));
    });
    const event = new WebSocket(`ws://localhost:${server.address.port}`, {
      headers: { 'X-Self-ID': '111', 'X-Client-Role': 'Event' },
    });
    event.on('open', () => {
      event.send(JSON.stringify(payload));
    });
  });

  const registry = new CommandRegistry(server);

  const dice = {
    name: 'dice',
    aliases: ['roll'],

    process(convo: Convo) {
      // chosen by fair dice roll, guaranteed to be random
      expect(convo.event).toEqual(payload);
      convo.reply('4').then(() => server.close(done));
    },
  };
  registry.add(dice);
});
