import { MsgEvent } from '../cqapi';
import { Server } from '../server';
import { Client } from '../client';
import { Command } from './command';
import { Convo } from './convo';

export interface CommandRegistryOptions {
  /**
   * The prefixes that can be used to invoke an command
   * @default ['.']
   */
  prefixes?: string[];
  /**
   * The time to wait before an user's reply is resolved as undefined
   * @default 300000
   */
  timeout?: number;
}

/**
 * A helper class that can be used to handle bot commands
 */
export class CommandRegistry {
  prefixes: string[];
  timeout: number;

  private _commands: Map<string, Command> = new Map();
  private _resQueue: Map<number, (event: MsgEvent) => void> = new Map();

  constructor(server: Server, options?: CommandRegistryOptions);
  constructor(
    server: Server,
    { prefixes = ['.'], timeout = 5 * 60 * 1000 }: CommandRegistryOptions = {},
  ) {
    this.prefixes = prefixes;
    this.timeout = timeout;

    server.on('message', (client, event) => {
      const exec = this._resQueue.get(event.user_id);
      if (exec) {
        this._resQueue.delete(event.user_id);
        exec(event);
      } else {
        this.handleCmd(client, event);
      }
    });
  }

  /** Adds or updates a command to the `Map` containing all the commands */
  add(cmd: Command) {
    if (cmd.aliases) {
      cmd.aliases.forEach(v => this._commands.set(v, cmd));
    }
    this._commands.set(cmd.name, cmd);
  }

  private handleCmd(client: Client, event: MsgEvent): void {
    // match against prefixes
    const prefix = this.prefixes.find(prefix =>
      event.raw_message.startsWith(prefix),
    );
    if (!prefix) return;

    // match against commands
    const args = event.raw_message.replace(prefix, '').split(' ');
    const cmdName = args.shift();
    if (!cmdName) return;

    const cmd = this._commands.get(cmdName);
    if (!cmd) return;

    cmd.process(new Convo(client, event, this._resQueue, this.timeout), args);
  }
}
