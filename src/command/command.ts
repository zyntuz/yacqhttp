import { Convo } from './convo';

export interface Command {
  /** The unique command name that can be used to invoke this command */
  name: string;
  /** This method will be called when the command is invoked */
  process(convo: Convo, args: string[]): void;
  /** Other names that can be used to invoke this command */
  aliases?: string[];
}
