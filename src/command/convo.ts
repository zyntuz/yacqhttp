import { Client } from '../client';
import { MsgEvent, CqApiMsg, CqEventMsg } from '../cqapi';

export class Convo<T = any> {
  constructor(
    public client: Client,
    public event: MsgEvent<T>,
    private _queue: Map<number, (event: MsgEvent) => void>,
    private _timeout: number,
  ) {}

  /** Alias for `event.user_id` */
  get userId() {
    return this.event.user_id;
  }

  /** Alias for `event.message` */
  get msg() {
    return this.event.message;
  }

  /** Sends a message back */
  reply(message: CqApiMsg, escaped = false) {
    return this.client.send(this.event, message, escaped);
  }

  /**
   * Sends a message back and the next message of the user will be returned in a
   * promise
   */
  ask(
    message: CqApiMsg,
    { userId = -1, escaped = false } = {},
  ): Promise<CqEventMsg<T> | undefined> {
    if (message) this.client.send(this.event, message, escaped);
    const user_id = userId > 0 ? userId : this.event.user_id;

    return new Promise(resolve => {
      const timer = setTimeout(() => {
        this._queue.delete(user_id);
        resolve(undefined);
      }, this._timeout);
      this._queue.set(user_id, (event: MsgEvent<T>) => {
        clearTimeout(timer);
        resolve(event.message);
      });
    });
  }
}
