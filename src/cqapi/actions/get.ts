/**
 * @module yacqhttp/cqapi
 */

import { User, ApiGroupUser } from '../user';
import { CqStatus } from '../status';
import { ApiResponse } from './misc';

export interface GetLoginInfoRes extends ApiResponse {
  data: {
    user_id: number;
    nickname: string;
  };
}

export interface GetStrangerInfoParams {
  user_id: number;
  no_cache?: boolean;
}

export interface GetStrangerInfoRes extends ApiResponse {
  data: User;
}

export interface GetGroupListRes extends ApiResponse {
  data: {
    group_id: number;
    group_name: string;
  };
}

export interface GetGroupMemberInfoParams {
  group_id: number;
  user_id: number;
  no_cache?: boolean;
}

export interface GetGroupMemberInfoRes extends ApiResponse {
  data: ApiGroupUser;
}

export interface GetGroupMemberListParams {
  group_id: number;
}

export interface GetGroupMemberListRes extends ApiResponse {
  data: ApiGroupUser[];
}

export interface GetCookiesRes extends ApiResponse {
  data: {
    cookies: string;
  };
}

export interface GetCsrfTokenRes extends ApiResponse {
  data: {
    token: string;
  };
}

export interface GetCredentialsRes extends ApiResponse {
  data: {
    cookies: string;
    token: string;
  };
}

export interface GetRecordParams {
  file: string;
  out_format: string;
  full_path?: boolean;
}

export interface GetImageParams {
  file: string;
}

export interface GetBinRes extends ApiResponse {
  data: {
    file: string;
  };
}

export interface GetStatusRes extends ApiResponse {
  data: CqStatus;
}

export interface GetVersionInfoRes extends ApiResponse {
  data: {
    coolq_directory: string;
    coolq_edition: string;
    plugin_version: string;
    plugin_build_number: number;
    plugin_build_configuration: string;
  };
}
