/**
 * @module yacqhttp/cqapi
 */

export interface ApiResponse {
  status: 'ok' | 'failed' | 'async';
  retcode: number;
  echo: string;
  data: any;
}

export interface DeleteMsgParams {
  message_id: number;
}

export interface CleanDataDirParams {
  data_dir: 'image' | 'record' | 'show' | 'bface';
}

export interface CanSendBinRes extends ApiResponse {
  data: {
    yes: boolean;
  };
}

export interface NullRes extends ApiResponse {
  data: null;
}
