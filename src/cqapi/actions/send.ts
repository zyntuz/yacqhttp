/**
 * @module yacqhttp/cqapi
 */

import { CqApiMsg } from '../message';
import { ApiResponse } from './misc';

export interface SendPrivateMsgParams {
  user_id: number;
  message: CqApiMsg;
  auto_escape?: boolean;
}

export interface SendGroupMsgParams {
  group_id: number;
  message: CqApiMsg;
  auto_escape?: boolean;
}

export interface SendDiscussMsgParams {
  discuss_id: number;
  message: CqApiMsg;
  auto_escape?: boolean;
}

export interface SendMsgParams {
  message_type?: 'private' | 'group' | 'discuss';
  user_id?: number;
  discuss_id?: number;
  group_id?: number;
  message: CqApiMsg;
  auto_escape?: boolean;
}

export interface SendMsgRes extends ApiResponse {
  data: {
    message_id: number;
  };
}

export interface SendLikeParams {
  user_id: number;
  times?: number;
}
