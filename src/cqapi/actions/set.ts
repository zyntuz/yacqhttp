/**
 * @module yacqhttp/cqapi
 */

import { Anonymous } from '../user';

export interface SetGroupKickParams {
  group_id: number;
  user_id: number;
  reject_add_request?: boolean;
}

export interface SetGroupBanParams {
  group_id: number;
  user_id: number;
  duration: number;
}

export interface SetGroupAnonymousBanParams {
  group_id: number;
  anonymous?: Anonymous;
  anonymous_flag?: string;
  flag?: string;
  duration: number;
}

export interface SetGroupWholeBanParams {
  group_id: number;
  enable?: boolean;
}

export interface SetGroupAdminParams {
  group_id: number;
  user_id: number;
  enable?: boolean;
}

export interface SetGroupAnonymousParams {
  group_id: number;
  enable?: boolean;
}

export interface SetGroupCardParams {
  group_id: number;
  user_id: number;
  card?: string;
}

export interface SetGroupLeaveParams {
  group_id: number;
  is_dismiss?: boolean;
}

export interface SetGroupSpecialTitleParams {
  group_id: number;
  user_id: number;
  special_title?: string;
  duration?: number;
}

export interface SetDiscussLeaveParams {
  discuss_id: number;
}

export interface SetFriendAddRequestParams {
  flag: string;
  approve?: boolean;
  remark?: string;
}

export interface SetGroupAddRequestParams {
  flag: string;
  sub_type?: 'add' | 'invite';
  type?: 'add' | 'invite';
  approve?: boolean;
  reason?: string;
}

export interface SetRestartPluginParams {
  delay?: number;
}
