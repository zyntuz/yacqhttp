/**
 * @module yacqhttp/cqapi
 */

export interface BaseEvent {
  post_type: 'message' | 'notice' | 'request' | 'meta_event';
  time: number;
  self_id: number;
}
