/**
 * @module yacqhttp/cqapi
 */

import { MsgEvent } from './message';
import { MetaEvent } from './meta';
import { NoticeEvent } from './notice';
import { RequestEvent } from './request';

export type Event = MsgEvent | MetaEvent | NoticeEvent | RequestEvent;
