/**
 * @module yacqhttp/cqapi
 */

import { User, Anonymous, GroupUser } from '../user';
import { CqEventMsg } from '../message';
import { BaseEvent } from './base';

export interface BaseMsgEvent<T = any> extends BaseEvent {
  post_type: 'message';
  message_type: 'discuss' | 'group' | 'private';
  message_id: number;
  message: CqEventMsg<T>;
  raw_message: string;
  user_id: number;
  font: number;
  sender: User;
}

export interface DiscussMsgEvent<T = any> extends BaseMsgEvent<T> {
  message_type: 'discuss';
  discuss_id: number;
}

export interface GroupMsgEvent<T = any> extends BaseMsgEvent<T> {
  message_type: 'group';
  sub_type: 'normal' | 'anonymous' | 'notice';
  group_id: number;
  anonymous: Anonymous | null;
  sender: GroupUser;
}

export interface PrivateMsgEvent<T = any> extends BaseMsgEvent<T> {
  message_type: 'private';
  sub_type: 'friend' | 'group' | 'discuss' | 'other';
}

export type MsgEvent<T = any> =
  | DiscussMsgEvent<T>
  | GroupMsgEvent<T>
  | PrivateMsgEvent<T>;
