/**
 * @module yacqhttp/cqapi
 */

import { CqStatus } from '../status';
import { BaseEvent } from './base';

export interface MetaEvent extends BaseEvent {
  post_type: 'meta_event';
  meta_event_type: 'lifecycle' | 'heartbeat';
}

export interface LifecycleMetaEvent extends MetaEvent {
  meta_event_type: 'lifecycle';
  sub_type: 'enable' | 'disable';
}

export interface HeartbeatMetaEvent extends MetaEvent {
  meta_event_type: 'heartbeat';
  status: CqStatus;
}
