/**
 * @module yacqhttp/cqapi
 */

import { GroupFile } from '../file';
import { BaseEvent } from './base';

export interface NoticeEvent extends BaseEvent {
  post_type: 'notice';
  notice_type:
    | 'group_upload'
    | 'group_admin'
    | 'group_decrease'
    | 'group_increase'
    | 'friend_add';
  user_id: number;
}

export interface GroupUploadNoticeEvent extends NoticeEvent {
  notice_type: 'group_upload';
  group_id: number;
  file: GroupFile;
}

export interface GroupAdminNoticeEvent extends NoticeEvent {
  notice_type: 'group_admin';
  sub_type: 'set' | 'unset';
  group_id: number;
}

export interface GroupDecreaseNoticeEvent extends NoticeEvent {
  notice_type: 'group_decrease';
  sub_type: 'leave' | 'kick' | 'kick_me';
  group_id: number;
  operator_id: number;
}

export interface GroupIncreaseNoticeEvent extends NoticeEvent {
  notice_type: 'group_increase';
  sub_type: 'approve' | 'invite';
  group_id: number;
  operator_id: number;
}

export interface FriendAddNoticeEvent extends NoticeEvent {
  notice_type: 'friend_add';
}
