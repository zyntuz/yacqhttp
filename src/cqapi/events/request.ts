/**
 * @module yacqhttp/cqapi
 */

import { BaseEvent } from './base';

export interface RequestEvent extends BaseEvent {
  post_type: 'request';
  request_type: 'friend' | 'group';
  user_id: number;
  comment: string;
  flag: string;
}

export interface FriendRequestEvent extends RequestEvent {
  request_type: 'friend';
}

export interface GroupRequestEvent extends RequestEvent {
  request_type: 'group';
  sub_type: 'add' | 'invite';
  group_id: number;
}
