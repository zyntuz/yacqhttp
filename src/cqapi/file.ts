/**
 * @module yacqhttp/cqapi
 */

export interface GroupFile {
  id: string;
  name: string;
  size: number;
  busid: number;
}
