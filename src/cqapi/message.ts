/**
 * @module yacqhttp/cqapi
 */

export interface CqCode {
  type: string;
  data: null | Record<string, string | undefined>;
}

export type CqEventMsg<T = any> = T extends string
  ? string
  : T extends Array<any>
  ? CqCode[]
  : string | CqCode[];

export type CqApiMsg = string | CqCode[] | CqCode;

export interface CqFace extends CqCode {
  type: 'face';
  data: {
    id: string;
  };
}

export interface CqEmoji extends CqCode {
  type: 'emoji';
  data: {
    id: string;
  };
}

export interface CqBface extends CqCode {
  type: 'bface';
  data: {
    id: string;
  };
}

export interface CqImage extends CqCode {
  type: 'image';
  data: {
    file: string;
    url?: string;
    cache?: string;
  };
}

export interface CqRecord extends CqCode {
  type: 'record';
  data: {
    file: string;
    magic?: 'true' | 'false';
  };
}

export interface CqAt extends CqCode {
  type: 'at';
  data: {
    qq: string;
  };
}

export interface CqRps extends CqCode {
  type: 'rps';
  data: {
    type?: string;
  };
}

export interface CqDice extends CqCode {
  type: 'dice';
  data: {
    type?: string;
  };
}

export interface CqShake extends CqCode {
  type: 'shake';
}

export interface CqAnonymous extends CqCode {
  type: 'anonymous';
  data: {
    ignore?: 'true' | 'false';
  };
}

export interface CqMusic extends CqCode {
  type: 'music';
  data: {
    type: 'qq' | '163' | 'xiami';
    id: string;
  };
}

export interface CqCustomMusic extends CqCode {
  type: 'music';
  data: {
    type: 'custom';
    url: string;
    audio: string;
    title: string;
    content?: string;
    image?: string;
  };
}

export interface CqShare extends CqCode {
  type: 'share';
}
