/**
 * @module yacqhttp/cqapi
 */

export interface Anonymous {
  id: number;
  name: string;
  flag: string;
}

export interface User {
  user_id: number;
  nickname: string;
  sex: 'male' | 'female' | 'unknown';
  age: number;
}

export interface GroupUser extends User {
  card: string;
  area: string;
  level: string;
  role: 'owner' | 'admin' | 'member';
  title: string;
}

export interface ApiGroupUser extends GroupUser {
  group_id: number;
  join_time: number;
  last_sent_time: number;
  unfriendly: boolean;
  title_expire_time: number;
  card_changeable: boolean;
}
