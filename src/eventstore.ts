import { EventEmitter } from 'events';

/**
 * A helper for the event emitter for internal use
 * @ignore
 */
export class EventStore<T> {
  private emitter = new EventEmitter();

  constructor(private timeout: number) {}

  add(eventName: string, event: T) {
    this.emitter.emit(eventName, event);
  }

  async get(eventName: string): Promise<T> {
    return new Promise((resolve, reject) => {
      const listener = (res: T) => {
        clearTimeout(timer);
        resolve(res);
      };
      const timer = setTimeout(() => {
        this.emitter.off(eventName, listener);
        reject('timeout');
      }, this.timeout);
      this.emitter.once(eventName, listener);
    });
  }
}
