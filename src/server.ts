import fs from 'fs';
import http from 'http';
import https from 'https';
import { EventEmitter } from 'events';
import WebSocket from 'ws';

import { EventStore } from './eventstore';
import { Client } from './client';
import { ApiResponse } from './cqapi';
import { Events } from './types';

function getKeyString(value: string | string[] | undefined): string {
  if (typeof value === 'string') return value.toLowerCase();
  else return '';
}

function createVerifyClient(token?: string) {
  if (!token) return undefined;
  return (info: any) => {
    const value = getKeyString(info.req.headers['authorization']);
    return value.substr(value.indexOf(' ') + 1) === token.toLowerCase();
  };
}

/**
 * Options for a CqHttp server
 */
export interface ServerOptions {
  /**
   * used to validate the Authorization header if provided
   * @default undefined
   */
  token?: string;
  /**
   * The path of the cert chain in PEM format
   * @default undefined
   */
  cert?: string;
  /**
   * The path of the private key in PEM format
   * @default undefined
   */
  key?: string;
  /**
   * The time to wait before an [[ApiResponse]] is rejected in ms
   * @default 30000
   */
  timeout?: number;
}

/** See https://github.com/websockets/ws/blob/master/doc/ws.md#serveraddress */
type AddressInfo =
  | string
  | {
      address: string;
      family: string;
      port: number;
    };

/**
 * The main hub to interact with the CQHTTP API where a ws server is created
 */
export interface Server extends EventEmitter {
  /** Adds the `listener` function for the `event` */
  addListener<K extends keyof Events>(event: K, listener: Events[K]): this;
  /** Adds the `listener` function for the `event` */
  on<K extends keyof Events>(event: K, listener: Events[K]): this;
  /** Adds a one-time `listener` function for the `event` */
  once<K extends keyof Events>(event: K, listener: Events[K]): this;
  /** Removes the specified `listener` */
  removeListener<K extends keyof Events>(event: K, listener: Events[K]): this;
  /** Removes the specified `listener` */
  off<K extends keyof Events>(event: K, listener: Events[K]): this;
}

export class Server extends EventEmitter {
  private server: https.Server | http.Server;
  private wsServer: WebSocket.Server;
  private echoId: number = 0;

  /** A map of all the CoolQ instances */
  clients: Map<number, Client> = new Map();

  /** Used to get server response for API calls */
  apiStore: EventStore<ApiResponse>;

  /** Returns a fairly unique string that is used for `echo` field */
  get echo() {
    if (this.echoId > 9999990) this.echoId = 0;
    else ++this.echoId;
    return this.echoId + '';
  }

  /**
   * Returns an object with `port`, `family`, and `address` properties
   * specifying the bound address if listening on an IP socket.
   * Returns the name as a string if listening on a pipe or UNIX domain socket.
   */
  get address(): AddressInfo {
    return this.wsServer.address();
  }

  constructor(options?: ServerOptions);
  constructor({ token, cert, key, timeout = 30000 }: ServerOptions = {}) {
    super();

    this.apiStore = new EventStore(timeout);
    this.server =
      cert && key
        ? https.createServer({
            cert: fs.readFileSync(cert),
            key: fs.readFileSync(key),
          })
        : http.createServer();

    this.wsServer = new WebSocket.Server({
      server: this.server,
      verifyClient: createVerifyClient(token),
    });
    this.wsServer.on('error', err => this.emit('socket.error', err));
    this.wsServer.on('connection', (ws, req) => {
      const role = getKeyString(req.headers['x-client-role']);
      const id = parseInt(getKeyString(req.headers['x-self-id']));
      if (isNaN(id)) return ws.terminate();

      ws.on('error', err => this.emit('socket.error', err));
      ws.on('close', (code, reason) => {
        this.emit('socket.close', role, code, reason);
      });

      switch (role) {
        case 'api':
          if (!this.clients.has(id)) {
            this.clients.set(id, new Client(id, this));
          }
          this.clients.get(id)!.addApi(ws);
          this.emit('socket.open', role);
          break;
        case 'event':
          if (!this.clients.has(id)) {
            this.clients.set(id, new Client(id, this));
          }
          this.clients.get(id)!.addEvent(ws);
          this.emit('socket.open', role);
          break;
        default:
          ws.terminate();
          return;
      }

      if (this.clients.get(id)!.ready) this.emit('ready', this.clients.get(id));
    });
  }

  /**
   * Starts the server listening for connections
   * @param cb called when the server is listening
   */
  listen(host: string, port: number, cb?: () => void) {
    if (cb) this.once('socket.listening', cb);
    this.server.listen({ host, port }, () => this.emit('socket.listening'));
    return this;
  }

  /**
   * Stops the server from listening and closes existing connections
   */
  close(cb?: (err?: Error) => void) {
    this.clients.clear();
    this.server.close();
    this.wsServer.close(cb);
  }
}
