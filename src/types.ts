import * as d from './cqapi';
import { Client } from './client';

// prettier-ignore
/**
 * See [CQHTTP API's official documentation](https://cqhttp.cc/docs/#/Post)
 */
export interface Events {
  'socket.open': (url: string) => void;
  'socket.error': (err: Error) => void;
  'socket.close': (url: string, code: number, reason: string) => void;
  'socket.listening': () => void;

  ready: (client: Client) => void;

  message: (client: Client, event: d.MsgEvent) => void;
  'message.discuss': (client: Client, event: d.DiscussMsgEvent) => void;
  'message.group': (client: Client, event: d.GroupMsgEvent) => void;
  'message.group.normal': (client: Client, event: d.GroupMsgEvent) => void;
  'message.group.anonymous': (client: Client, event: d.GroupMsgEvent) => void;
  'message.group.notice': (client: Client, event: d.GroupMsgEvent) => void;
  'message.private': (client: Client, event: d.PrivateMsgEvent) => void;
  'message.private.friend': (client: Client, event: d.PrivateMsgEvent) => void;
  'message.private.group': (client: Client, event: d.PrivateMsgEvent) => void;
  'message.private.discuss': (client: Client, event: d.PrivateMsgEvent) => void;
  'message.private.other': (client: Client, event: d.PrivateMsgEvent) => void;

  meta_event: (client: Client, event: d.MetaEvent) => void;
  'meta_event.lifecycle': (client: Client, event: d.LifecycleMetaEvent) => void;
  'meta_event.lifecycle.enable': (client: Client, event: d.LifecycleMetaEvent) => void;
  'meta_event.lifecycle.disable': (client: Client, event: d.LifecycleMetaEvent) => void;
  'meta_event.heartbeat': (client: Client, event: d.HeartbeatMetaEvent) => void;

  notice: (client: Client, event: d.NoticeEvent) => void;
  'notice.group_upload': (client: Client, event: d.GroupUploadNoticeEvent) => void;
  'notice.group_admin': (client: Client, event: d.GroupAdminNoticeEvent) => void;
  'notice.group_admin.set': (client: Client, event: d.GroupAdminNoticeEvent) => void;
  'notice.group_admin.unset': (client: Client, event: d.GroupAdminNoticeEvent) => void;
  'notice.group_decrease': (client: Client, event: d.GroupDecreaseNoticeEvent) => void;
  'notice.group_decrease.leave': (client: Client, event: d.GroupDecreaseNoticeEvent) => void;
  'notice.group_decrease.kick': (client: Client, event: d.GroupDecreaseNoticeEvent) => void;
  'notice.group_decrease.kick_me': (client: Client, event: d.GroupDecreaseNoticeEvent) => void;
  'notice.group_increase': (client: Client, event: d.GroupIncreaseNoticeEvent) => void;
  'notice.group_increase.approve': (client: Client, event: d.GroupIncreaseNoticeEvent) => void;
  'notice.group_increase.invite': (client: Client, event: d.GroupIncreaseNoticeEvent) => void;
  'notice.friend_add': (client: Client, event: d.FriendAddNoticeEvent) => void;

  request: (client: Client, event: d.RequestEvent) => void;
  'request.friend': (client: Client, event: d.FriendRequestEvent) => void;
  'request.group': (client: Client, event: d.GroupRequestEvent) => void;
  'request.group.add': (client: Client, event: d.GroupRequestEvent) => void;
  'request.group.invite': (client: Client, event: d.GroupRequestEvent) => void;
}

// prettier-ignore
/**
 * See [CQHTTP API's official documentation](https://cqhttp.cc/docs/#/API)
 */
export interface Actions {
  send_private_msg: [d.SendPrivateMsgParams, d.SendMsgRes];
  send_group_msg: [d.SendGroupMsgParams, d.SendMsgRes];
  send_discuss_msg: [d.SendDiscussMsgParams, d.SendMsgRes];
  send_msg: [d.SendMsgParams, d.SendMsgRes];
  delete_msg: [d.DeleteMsgParams, d.NullRes];
  send_like: [d.SendLikeParams, d.NullRes];
  set_group_kick: [d.SetGroupKickParams, d.NullRes];
  set_group_ban: [d.SetGroupBanParams, d.NullRes];
  set_group_anonymous_ban: [d.SetGroupAnonymousBanParams, d.NullRes];
  set_group_whole_ban: [d.SetGroupWholeBanParams, d.NullRes];
  set_group_admin: [d.SetGroupAdminParams, d.NullRes];
  set_group_anonymous: [d.SetGroupAnonymousParams, d.NullRes];
  set_group_card: [d.SetGroupCardParams, d.NullRes];
  set_group_leave: [d.SetGroupLeaveParams, d.NullRes];
  set_group_special_title: [d.SetGroupSpecialTitleParams, d.NullRes];
  set_discuss_leave: [d.SetDiscussLeaveParams, d.NullRes];
  set_friend_add_request: [d.SetFriendAddRequestParams, d.NullRes];
  set_group_add_request: [d.SetGroupAddRequestParams, d.NullRes];
  get_login_info: [undefined, d.GetLoginInfoRes];
  get_stranger_info: [d.GetStrangerInfoParams, d.GetStrangerInfoRes];
  get_group_list: [undefined, d.GetGroupListRes];
  get_group_member_info: [d.GetGroupMemberInfoParams, d.GetGroupMemberInfoRes];
  get_group_member_list: [d.GetGroupMemberListParams, d.GetGroupMemberListRes];
  get_cookies: [undefined, d.GetCookiesRes];
  get_csrf_token: [undefined, d.GetCsrfTokenRes];
  get_credentials: [undefined, d.GetCredentialsRes];
  get_record: [d.GetRecordParams, d.GetBinRes];
  get_image: [d.GetImageParams, d.GetBinRes];
  can_send_image: [undefined, d.CanSendBinRes];
  can_send_record: [undefined, d.CanSendBinRes];
  get_status: [undefined, d.GetStatusRes];
  get_version_info: [undefined, d.GetVersionInfoRes];
  set_restart_plugin: [d.SetRestartPluginParams, d.NullRes];
  clean_data_dir: [d.CleanDataDirParams, d.NullRes];
  clean_plugin_log: [undefined, d.NullRes];
}
